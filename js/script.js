const title = document.querySelector('.title')
const content = document.querySelector('.description')
const submitBtn = document.querySelector('.submitBtn')
const container = document.querySelector('.row')
// Добавление задач в localStorage если он пуст
window.addEventListener('load' , () => {
    if(!localStorage.getItem('todos')){
        localStorage.setItem('todos' , JSON.stringify([]))
    }else{
        const todos = JSON.parse(localStorage.getItem("todos"))
        const newTodos = todos.map((item , index) => {
            return { ...item , id: index}
        })
        localStorage.setItem('todos' , JSON.stringify(newTodos))
        const template = newTodos.reverse().reduce((prev , {title , content , id , completed , date}) => {
            if(completed){
                return prev + `<div class="col-lg-6 completed mb-4">${cardTemplate(title , content , date , id)}</div>`
            }else{
                return prev + `<div class="col-lg-6 mb-4">${cardTemplate(title , content , date , id)}</div>`  
            }
        }, '')
        container.innerHTML = template
    }
})
// Adding new Task
submitBtn.addEventListener('click' , e => {
    e.preventDefault();
    if(title.value ===  '' && content.value === ''){
        alert("Поля не должна быть пустыми"); 
    }
    if(title.value !== ' ' && content.value !== ' '){
        const todos = JSON.parse(localStorage.getItem('todos'))
        localStorage.setItem('todos' , JSON.stringify([
            ...todos,
            {
                title: title.value,
                content: content.value,
                date: CurrentTime(),
                completed: false,
            }
        ]));
        window.location.reload();
    }
})
// Card Templete with a length checker
function cardTemplate(title , content , time , id){
    if(content.length >= 320){
        return `
            <div class="big_block_cards">
                <div class="cont_card">
                    <div class="card-header">
                        <h3 class="card-title">${title}</h3>
                    </div>
                    <div class="card-body content shorted">
                        <p>${content}</p>
                        <span class="time">${time}</span>
                    </div>
                    <div class="card-header">
                        <button onclick='deleteTask(${id})' class="btn b_1" data-id="${id}">Delete</button>
                        <button onclick='completeTask(${id})' class="btn b_2" data-id="${id}">Complite</button>
                        <button onclick='editTask(${id})' class="btn b_3" data-id="${id}">Edit</button>
                    </div>
                </div>
            </div>
        `
    }else{
        return `
            <div class="big_block_cards">
                <div class="cont_card">
                    <div class="card-header">
                        <h3 class="card-title">${title}</h3>
                    </div>
                    <div class="card-body content">
                        <p>${content}</p>
                        <span class="time">${time}</span>
                    </div>
                    <div class="card-header">
                        <button onclick='deleteTask(${id})' class="btn b_1" data-id="${id}">Delete</button>
                        <button onclick='completeTask(${id})' class="btn b_2" data-id="${id}">Complite</button>
                        <button onclick='editTask(${id})' class="btn b_3" data-id="${id}">Edit</button>
                    </div>
                </div>
            </div>
        `
    }
}
// Get Current Time
function CurrentTime(){
    return `${moment().format('L')} ${moment().format('LTS')}`
}
// Change Theme with a localStorage
const body = document.body;
const selector = document.querySelector('.theme-selector')
selector.addEventListener('change' , e => {
    e.preventDefault();
    const value = e.target.value
    if(value === 'dark'){
        body.style.background = '#000'
        localStorage.setItem('themeColor' , '#000')
        localStorage.setItem('theme' , 'dark')
    }else if(value === 'light'){
        body.style.background = '#585858'
        localStorage.setItem('themeColor' , '#606060')
        localStorage.setItem('theme' , 'light')
    }else if(value === 'custom'){
        const askTheme = prompt("Your custom color? (hex)")
        body.style.background = askTheme
        localStorage.setItem('themeColor' , askTheme)
        localStorage.setItem('theme' , 'custom')
    }
})
window.addEventListener('load' , () => {
    if(localStorage.getItem('theme')){
        body.style.background = localStorage.getItem('themeColor')
        selector.value = localStorage.getItem('theme')
    }
})
// Delete Task
function deleteTask(id){
    const askDelete = confirm("You u sure?")
    if(!askDelete) return;
    const todos = JSON.parse(localStorage.getItem('todos'))
    const newTodos = todos.filter(item => item.id !== id)
    localStorage.setItem('todos' , JSON.stringify(newTodos))
    window.location.reload();
}
// Complete Task
function completeTask(id){
    const todos = JSON.parse(localStorage.getItem('todos'))
    const newTodos = todos.map(item => {
        if(item.id === id){
            return {
                ...item,
                completed: !item.completed
            }
        }else{
            return item
        }
    })
    localStorage.setItem('todos' , JSON.stringify(newTodos))
    window.location.reload()
}
// Edit task
function editTask(id){
    const todos = JSON.parse(localStorage.getItem('todos'))
    const newTodos = todos.map(item => {
        if(item.id === id){
            return {
                ...item,
                title: prompt("New Title" , item.title),
                content: prompt("New Context" , item.content)
            }
        }else{
            return item
        }
    })
    localStorage.setItem('todos' , JSON.stringify(newTodos))
    window.location.reload()
}


window.addEventListener('load', () => {
    const isAuth = localStorage.getItem('isAuth');
    isAuth === 'true' ? null : window.open('index.html' , "_self");
})

const signOutBtn = document.querySelector('.signOutBtn')

signOutBtn.addEventListener('click' , e => {
    e.preventDefault();

    localStorage.setItem('isAuth', 'false');
    window.location.reload();
})